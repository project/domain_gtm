INTRODUCTION:
The Domain Google Tag Manager module provides option 
to configure the different GTM tags for each domain.

REQUIREMENTS:
Domain Access
GoogleTagManager

INSTALLATION:
Download and install the module as usual.

CONFIGURATION:
Go to module configuration and add the GTM ID for each domains.
